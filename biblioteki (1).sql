/*
SQLyog Community v13.1.2 (64 bit)
MySQL - 10.1.37-MariaDB : Database - biblioteki
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`biblioteki` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci */;

USE `biblioteki`;

/*Table structure for table `archiwum` */

DROP TABLE IF EXISTS `archiwum`;

CREATE TABLE `archiwum` (
  `id_archiwum` int(11) NOT NULL AUTO_INCREMENT,
  `id_ksiazki` int(11) DEFAULT NULL,
  `tytul` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `id_kategoria` int(11) DEFAULT NULL,
  `id_autor` int(11) DEFAULT NULL,
  `opis` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `id_wydawnictwo` int(11) DEFAULT NULL,
  `rokwydania` year(4) DEFAULT NULL,
  `cena` decimal(11,0) DEFAULT NULL,
  `data_archiwizacji` datetime DEFAULT NULL,
  `zlecajacy` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id_archiwum`),
  KEY `archiwum_ibfk_1` (`id_autor`),
  KEY `archiwum_ibfk_2` (`id_kategoria`),
  KEY `archiwum_ibfk_3` (`id_wydawnictwo`),
  KEY `id_ksiazki` (`id_ksiazki`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `archiwum` */

insert  into `archiwum`(`id_archiwum`,`id_ksiazki`,`tytul`,`id_kategoria`,`id_autor`,`opis`,`id_wydawnictwo`,`rokwydania`,`cena`,`data_archiwizacji`,`zlecajacy`) values 
(6,6,'qwe',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:22:15','Patryk@localhost'),
(7,7,'asd',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:22:15','Patryk@localhost'),
(8,8,'zxc',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:22:15','Patryk@localhost'),
(9,9,'wer',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:26:05','Patryk@localhost'),
(10,10,'dfg',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:26:52','Patryk@localhost'),
(11,11,'qwe',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:28:03','Patryk@localhost'),
(12,12,'asd',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:28:03','Patryk@localhost'),
(13,13,'zxc',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:28:36','Patryk@localhost'),
(14,14,'wer',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:30:50','Patryk@localhost'),
(15,15,'dfg',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:30:50','Patryk@localhost'),
(16,16,'qwe',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:32:12','root@localhost'),
(17,17,'asd',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:32:12','root@localhost'),
(18,18,'zxc',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:32:12','root@localhost'),
(19,19,'wer',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:32:12','root@localhost'),
(20,20,'dfg',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:32:12','root@localhost'),
(21,21,'qwe',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:56:12','Patryk@localhost'),
(22,22,'asd',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:56:12','Patryk@localhost'),
(23,23,'zxc',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:56:12','Patryk@localhost'),
(24,24,'wer',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:56:12','Patryk@localhost'),
(25,25,'dfg',NULL,NULL,NULL,NULL,NULL,0,'2019-02-12 14:56:12','Patryk@localhost');

/*Table structure for table `autorzy` */

DROP TABLE IF EXISTS `autorzy`;

CREATE TABLE `autorzy` (
  `id_autor` int(11) NOT NULL AUTO_INCREMENT,
  `imie` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `nazwisko` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_autor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `autorzy` */

insert  into `autorzy`(`id_autor`,`imie`,`nazwisko`) values 
(1,'jan','sebastian'),
(2,'piotr','dzban'),
(3,'grzesiu','doZABKI');

/*Table structure for table `filie` */

DROP TABLE IF EXISTS `filie`;

CREATE TABLE `filie` (
  `id_filie` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `adres` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `nr_telefonu` tinyint(9) NOT NULL,
  PRIMARY KEY (`id_filie`),
  UNIQUE KEY `nazwa` (`nazwa`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `filie` */

insert  into `filie`(`id_filie`,`nazwa`,`adres`,`nr_telefonu`) values 
(1,'qwe','',0),
(2,'asd','',0),
(3,'zxc','',0),
(4,'ert','',0);

/*Table structure for table `historia_ksiazek` */

DROP TABLE IF EXISTS `historia_ksiazek`;

CREATE TABLE `historia_ksiazek` (
  `id_historia` int(11) NOT NULL AUTO_INCREMENT,
  `id_ksiazki` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id_historia`),
  KEY `historia_ksiazek_ibfk_2` (`id_status`),
  KEY `id_ksiazki` (`id_ksiazki`),
  CONSTRAINT `historia_ksiazek_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `status_ksiazek` (`id_status`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `historia_ksiazek` */

insert  into `historia_ksiazek`(`id_historia`,`id_ksiazki`,`id_status`,`data`) values 
(2,9,2,'2019-02-12 14:26:05'),
(3,13,2,'2019-02-12 14:28:36'),
(4,14,1,'2019-02-12 14:28:50'),
(5,21,0,'2019-02-12 14:56:12'),
(6,22,0,'2019-02-12 14:56:12'),
(7,23,0,'2019-02-12 14:56:12'),
(8,24,0,'2019-02-12 14:56:12'),
(9,25,0,'2019-02-12 14:56:12');

/*Table structure for table `kategorie` */

DROP TABLE IF EXISTS `kategorie`;

CREATE TABLE `kategorie` (
  `id_kategorie` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_kategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `kategorie` */

insert  into `kategorie`(`id_kategorie`,`nazwa`) values 
(1,'Fantasy'),
(2,'Horror'),
(3,'Komedia');

/*Table structure for table `ksiazki` */

DROP TABLE IF EXISTS `ksiazki`;

CREATE TABLE `ksiazki` (
  `id_ksiazki` int(11) NOT NULL AUTO_INCREMENT,
  `tytul` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `id_kategoria` int(11) DEFAULT NULL,
  `id_autor` int(11) DEFAULT NULL,
  `opis` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `id_wydawnictwo` int(11) DEFAULT NULL,
  `rokwydania` year(4) DEFAULT NULL,
  `cena` decimal(4,0) NOT NULL,
  PRIMARY KEY (`id_ksiazki`),
  KEY `ksiazki_ibfk_1` (`id_autor`),
  KEY `ksiazki_ibfk_2` (`id_kategoria`),
  KEY `ksiazki_ibfk_3` (`id_wydawnictwo`),
  CONSTRAINT `ksiazki_ibfk_1` FOREIGN KEY (`id_autor`) REFERENCES `autorzy` (`id_autor`) ON DELETE SET NULL,
  CONSTRAINT `ksiazki_ibfk_2` FOREIGN KEY (`id_kategoria`) REFERENCES `kategorie` (`id_kategorie`) ON DELETE SET NULL,
  CONSTRAINT `ksiazki_ibfk_3` FOREIGN KEY (`id_wydawnictwo`) REFERENCES `wydawnictwa` (`id_wydawnictwo`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `ksiazki` */

/*Table structure for table `sprzedaz` */

DROP TABLE IF EXISTS `sprzedaz`;

CREATE TABLE `sprzedaz` (
  `id_sprzedaz` int(11) NOT NULL AUTO_INCREMENT,
  `id_ksiazki` int(11) DEFAULT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id_sprzedaz`),
  KEY `id_ksiazki` (`id_ksiazki`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `sprzedaz` */

insert  into `sprzedaz`(`id_sprzedaz`,`id_ksiazki`,`data`) values 
(2,9,'2019-02-12 14:26:05'),
(3,13,'2019-02-12 14:28:36');

/*Table structure for table `status_ksiazek` */

DROP TABLE IF EXISTS `status_ksiazek`;

CREATE TABLE `status_ksiazek` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `status_ksiazek` */

insert  into `status_ksiazek`(`id_status`,`status`) values 
(0,'Dostępna'),
(1,'Wypożyczona'),
(2,'Sprzedana'),
(3,'Zarchwizowana');

/*Table structure for table `uzytkownicy` */

DROP TABLE IF EXISTS `uzytkownicy`;

CREATE TABLE `uzytkownicy` (
  `id_uzytkownicy` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `haslo` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `Grupa` int(11) NOT NULL,
  PRIMARY KEY (`id_uzytkownicy`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `uzytkownicy` */

insert  into `uzytkownicy`(`id_uzytkownicy`,`login`,`haslo`,`Grupa`) values 
(1,'Grzesiu',NULL,0);

/*Table structure for table `wydawnictwa` */

DROP TABLE IF EXISTS `wydawnictwa`;

CREATE TABLE `wydawnictwa` (
  `id_wydawnictwo` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id_wydawnictwo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `wydawnictwa` */

insert  into `wydawnictwa`(`id_wydawnictwo`,`nazwa`) values 
(1,'SOWA'),
(2,'BODY'),
(3,'KRAJST');

/*Table structure for table `wypozyczenia` */

DROP TABLE IF EXISTS `wypozyczenia`;

CREATE TABLE `wypozyczenia` (
  `id_wypozyczenia` int(11) NOT NULL AUTO_INCREMENT,
  `id_ksiazki` int(11) NOT NULL,
  `id_uzytkownicy` int(11) DEFAULT NULL,
  `data_wypozyczenia` date DEFAULT NULL,
  `data_oddania` date DEFAULT NULL,
  `id_filie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_wypozyczenia`),
  KEY `wypozyczenia_ibfk_1` (`id_filie`),
  KEY `wypozyczenia_ibfk_2` (`id_uzytkownicy`),
  KEY `wypozyczenia_ibfk_3` (`id_ksiazki`),
  CONSTRAINT `wypozyczenia_ibfk_1` FOREIGN KEY (`id_filie`) REFERENCES `filie` (`id_filie`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wypozyczenia_ibfk_2` FOREIGN KEY (`id_uzytkownicy`) REFERENCES `uzytkownicy` (`id_uzytkownicy`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wypozyczenia_ibfk_3` FOREIGN KEY (`id_ksiazki`) REFERENCES `ksiazki` (`id_ksiazki`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `wypozyczenia` */

/* Trigger structure for table `archiwum` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `archiwum` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `archiwum` AFTER INSERT ON `archiwum` FOR EACH ROW BEGIN
INSERT INTO historia_ksiazek(id_ksiazki,id_status,data) 
SELECT id_ksiazki,0,NOW() from archiwum
WHERE NEW.id_ksiazki = id_ksiazki;
END */$$


DELIMITER ;

/* Trigger structure for table `ksiazki` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `doarchiwum` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `doarchiwum` BEFORE DELETE ON `ksiazki` FOR EACH ROW INSERT INTO archiwum(id_ksiazki,tytul,id_kategoria,id_autor,opis,id_wydawnictwo,rokwydania,cena,data_archiwizacji,zlecajacy) 
SELECT id_ksiazki,tytul,id_kategoria,id_autor,opis,id_wydawnictwo,rokwydania,cena,NOW(),USER() from ksiazki WHERE OLD.id_ksiazki = id_ksiazki */$$


DELIMITER ;

/* Trigger structure for table `sprzedaz` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `datasprzedazy` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `datasprzedazy` BEFORE INSERT ON `sprzedaz` FOR EACH ROW BEGIN 
SET NEW.data = NOW();
END */$$


DELIMITER ;

/* Trigger structure for table `sprzedaz` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `sprzedaz` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `sprzedaz` AFTER INSERT ON `sprzedaz` FOR EACH ROW BEGIN

INSERT INTO historia_ksiazek(id_ksiazki,id_status,data) 
SELECT id_ksiazki,2,NOW() from sprzedaz
WHERE NEW.id_ksiazki = id_ksiazki;

DELETE FROM ksiazki WHERE
NEW.id_ksiazki = id_ksiazki;

END */$$


DELIMITER ;

/* Trigger structure for table `wypozyczenia` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `datawypozcz` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `datawypozcz` BEFORE INSERT ON `wypozyczenia` FOR EACH ROW SET NEW.data_wypozyczenia = NOW(), NEW.data_oddania = NOW() + INTERVAL 1 MONTH */$$


DELIMITER ;

/* Trigger structure for table `wypozyczenia` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `wypozyczenie` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `wypozyczenie` AFTER INSERT ON `wypozyczenia` FOR EACH ROW BEGIN
INSERT INTO historia_ksiazek(id_ksiazki,id_status,data) 
SELECT id_ksiazki,1,NOW() from wypozyczenia
WHERE NEW.id_ksiazki = id_ksiazki;

END */$$


DELIMITER ;

/* Function  structure for function  `dodajksiazki` */

/*!50003 DROP FUNCTION IF EXISTS `dodajksiazki` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `dodajksiazki`() RETURNS int(11)
    MODIFIES SQL DATA
BEGIN
INSERT INTO ksiazki(tytul) VALUES('qwe'),('asd'),('zxc'),('wer'),('dfg');
RETURN 0;
END */$$
DELIMITER ;

/* Procedure structure for procedure `archiwizacja` */

/*!50003 DROP PROCEDURE IF EXISTS  `archiwizacja` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `archiwizacja`(IN `arch1` INT, IN `arch2` INT)
    MODIFIES SQL DATA
BEGIN
SET @archi1 = arch1;
SET @archi2 = arch2;

DELETE FROM ksiazki WHERE ksiazki.id_ksiazki BETWEEN @archi1 AND @archi2;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
